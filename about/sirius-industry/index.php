<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сириус Индастри");
?>

<section class="company-info company-info--sirius">
	<div class="company-info__container">
		<picture class="company-info__logo">
			<img src="/img/holding-logo.jpg">
		</picture>
		<h1 class="company-info__title">Сириус Индастри</h1>

		<b class="company-info__highlight">Наша Цель:</b>
		<p class="company-info__text">
			«Чтобы каждый человек в России мог позволить себе качественную продукцию Российского и зарубежного производства во всех сферах потребления по доступным ценам.»
		</p>
		<b class="company-info__highlight">Наша Миссия:</b>
		<p class="company-info__text">
			«Совершенствования и развития рынка дистрибуции товаров для населения»
		</p>
	</div>
</section>

<section class="features features--holding-sirius">
	<div class="features__container">
		<h3 class="features__title">Наши правила и принципы:</h3>
		<ul class="features__list features__list--holding-sirius">
			<li class="features__item features__item--holding-sirius">
				<p class="features__text">Прежде всего конечный потребитель. Это то, ради чего мы развиваемся. Каждый раз когда
мы принимаем то или иное решение, выбирая из нескольких вариантов, мы просто
спрашиваем себя: а какой больше поможет людям? И выбираем именно его.</p>
			</li>
			<li class="features__item features__item--holding-sirius">
				<p class="features__text">
					Открытость и честность перед людьми.
				</p>
			</li>
			<li class="features__item features__item--holding-sirius">
				<p class="features__text">
					Простота в общении, желание выслушать и понять наших партнеров. Мы говорим простым
и понятным языком.
				</p>
			</li>
			<li class="features__item features__item--holding-sirius">
				<p class="features__text">
					Не бежать за лидером, а делать так, чтобы нас догоняли. Мы ценим не стандартный
подход к решению поставленных задач, мы не идем путем который повторяют друг за другом
другие компании, мы создаем новый нестандартный подход к дистрибуции. И собираемся
продолжать работу на рынке так, чтобы конкурентам оставалось только удивляться, почему
они не делали так же раньше.
				</p>
			</li>
		</ul>
	</div>
</section>

<section class="inner-heading">
	<div class="inner-heading__container">
		<h1 class="inner-heading__title">Профиль деятельности:</h1>
		<p class="inner-heading__text">
			<b>Коммерческая дистрибуция.</b>
			<br>
			Торговый дом Сириус является поставщиков в крупнейшие оптовые фармацевтические
			компании, федеральные и региональные фармацевтические сети, торговые сети.
			Среди наших клиентов: - далее полоса прокрутки с логотипами
		</p>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>