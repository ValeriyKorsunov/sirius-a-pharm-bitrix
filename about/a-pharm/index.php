<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("А-ФАРМ");
?>

<section class="company-info company-info--pharm">
	<div class="company-info__container">
		<picture class="company-info__logo">
			<source type="image/webp" srcset="/img/holding-logo-pharm.webp 1x, /img/holding-logo-pharm@2x.webp 2x, /img/holding-logo-pharm@3x.webp 3x">
			<img src="/img/holding-logo-pharm.png" srcset="/img/holding-logo-pharm@2x.png 2x, /img/holding-logo-pharm@3x.png 3x" width="193" height="50" alt="Логотип компании «A-Pharm»">
		</picture>
		<h1 class="company-info__title">А-ФАРМ</h1>
		<p class="company-info__direction">Основное направление деятельности компании – ответственное хранение&nbsp;медикаментов</p>
		<p class="company-info__represent company-info__represent--pharm">В этой сфере коллектив А-Фарм имеет более 20 лет богатейшего опыта.<br> За это время организация зарекомендовала себя как надежный партнер в фармацевтической индустрии.</p>
		<p class="company-info__text company-info__text--pharm">Компания предоставляет полный спектр услуг по складской логистике, а также по продаже лекпрепаратов оптом и в розницу. Клиенты и партнеры организации – крупнейшие компании отечественно и зарубежного рынка товаров армацевтики и медицинского назначение.</p>
		<p class="company-info__text company-info__text--pharm">Коллектив компании считает главной задачей создавать все условия для комфортного сотрудничества с А-Фарм, приоритетами в работе являются качество и надежность. Обширный опыт позволяет сотрудникам организации понимать и предвосхищать потребности клиентов, они готовы делиться своим опытом с новичками на рынке лексредств, помогая выстраивать успешную стратегию реализации лекарственных препаратов и находить эффективные решения для успешного развития.</p>
		<b class="company-info__highlight">За каждым клиентом компании А-Фарм закрепляется персональный менеджер.</b>
	</div>
</section>

<section class="features features--holding-pharm">
	<div class="features__container">
		<h3 class="features__title">Преимущества работы с компанией А-Фарм</h3>
		<ul class="features__list features__list--holding-pharm">
			<li class="features__item features__item--holding-pharm">
				<p class="features__text">Полный спектр логистических услуг. Прием товара, комплектование грузов, доставка клиенту и многое другое.</p>
			</li>
			<li class="features__item features__item--holding-pharm">
				<p class="features__text">Хранение грузов осуществляется на собственном лицензированном складе, с строгим соблюдением температурного режима и иных условий складирования.</p>
			</li>
			<li class="features__item features__item--holding-pharm">
				<p class="features__text">Компания несет полную материальную ответственность за сохранность доверенных ей грузов, оказывает услуги страхования.</p>
			</li>
			<li class="features__item features__item--holding-pharm">
				<p class="features__text">Широкий ассортимент компании и отточенные бизнес-процессы позволяют осуществлять отгрузку и отправку заказанного товара не более чем через 1 рабочий день (как правило, в день заказа).</p>
			</li>
			<li class="features__item features__item--holding-pharm">
				<p class="features__text">Для всех партнеров – привлекательные цены, для постоянных клиентов – гибкая система скидок.</p>
			</li>
			<li class="features__item features__item--holding-pharm">
				<p class="features__text">Наши склады имеют удобное расположение и собственную охраняемую стоянку, рассчитанную в том числе и на большегрузный транспорт. На территории ведется видеонаблюдение, охрана – круглосуточно. Все для гарантии полной безопасности хранения Вашего товара.</p>
			</li>
		</ul>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>