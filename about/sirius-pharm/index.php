<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сириус Фарм");
?>

<section class="company-info company-info--sirius">
		<div class="company-info__container">
			<picture class="company-info__logo">
				<source type="image/webp" srcset="/img/holding-logo-sirius.webp 1x, /img/holding-logo-sirius@2x.webp 2x, /img/holding-logo-sirius@3x.webp 3x">
				<img src="/img/holding-logo-sirius.png" srcset="/img/holding-logo-sirius@2x.png 2x, /img/holding-logo-sirius@3x.png 3x" width="143" height="48" alt="Логотип компании «Sirius»">
			</picture>
			<h1 class="company-info__title">Сириус Фарм</h1>
			<p class="company-info__direction">Основное направление деятельности: оптовая торговля медикаментами и сопутствующими товарами, а также дистрибуция и логистика</p>
			<p class="company-info__represent"><b>Кредо компании:</b> «Относись к человеку так, как ты хочешь, чтобы он относился к тебе». Именно поэтому компания&nbsp;Сириус Фарм известна своим исключительным вниманием к партнерам и заботой о конечном потребителе.</p>
			<p class="company-info__text">Коллектив компании Сириус Фарм верит, что искренне помогать людям можно не только адресно, но и в целом положительно влияя на индустрию, в которой организация является лидером – на рынок лекарственных препаратов и сопутствующих товаров на территории России. Другими словами, миссию компании можно обозначить как «Совершенствование и развитие отечественного рынка дистрибуции лекарств».</p>
			<b class="company-info__highlight">Особо хочется отметить обширный опыт сотрудников компании.</b>
			<p class="company-info__text">Каждый член этой слаженной команды успел поработать в разных сегментах отрасли, приобрести четкое понимание, что значит работать с хорошими или плохими поставщиками. Чтобы затем претворить в жизнь свое представление о поставщике, с которым действительно комфортно работать. Весь коллектив компании придерживается идеи построения корпоративной культуры и всех бизнес-процессов, основываясь на своем представлении о том, с каким поставщиком мы сами хотели бы вести дела.</p>
		</div>
	</section>

	<section class="features features--holding-sirius">
		<div class="features__container">
			<h3 class="features__title">Преимущества работы с компанией Сириус Фарм</h3>
			<ul class="features__list features__list--holding-sirius">
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Доступные цены.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Приоритеты компании – открытость и честность.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Стиль компании – простота в общении, отсутствие излишней бюрократии и готовность помочь в любом вопросе.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Каждому клиенту – одинаково исключительное внимание, даже самым маленьким партнерам и разовым клиентам.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Отгрузка товара осуществляется в самые короткие сроки.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Специалисты Сириус Фарм помогут разобраться во всех вопросах, касающихся реализации и дистрибуции медицинских товаров, дадут советы новичкам и смогут подарить свежие идеи даже самым опытным менеджерам.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Коллектив компании ежедневно работает над оптимизацией процессов и операций, сокращением сроков поставок, новыми решениями для типовых или нестандартных задач.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Менеджеры по работе с клиентами идут навстречу пожеланиям по поставкам, срокам и особенностям транспортировки.</p>
				</li>
			</ul>
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>