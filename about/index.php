<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О холдинге");
?>

	<section class="inner-heading">
		<div class="inner-heading__container">
			<h1 class="inner-heading__title">О группе компаний</h1>
			<p class="inner-heading__text">Группа компаний А-Фарм и Сириус Фарм  была создана в 2018 году. На момент объединения каждая из организаций уже имела более 20 лет богатого опыта в сфере продаж и дистрибуции медицинских товаров.  Сириус Фарм и А-Фарм удачно дополняют друг друга, предоставляя своим клиентам полный спектр услуг по реализации, хранению, логистике и дистрибуции медикаментов и товаров медицинского назначения.</p>
		</div>
	</section>

	<section class="company-info company-info--sirius">
		<div class="company-info__container">
			<picture class="company-info__logo">
				<source type="image/webp" srcset="/img/holding-logo-sirius.webp 1x, /img/holding-logo-sirius@2x.webp 2x, /img/holding-logo-sirius@3x.webp 3x">
				<img src="/img/holding-logo-sirius.png" srcset="/img/holding-logo-sirius@2x.png 2x, /img/holding-logo-sirius@3x.png 3x" width="143" height="48" alt="Логотип компании «Sirius»">
			</picture>
			<h2 class="company-info__title">Сириус Фарм</h2>
			<p class="company-info__direction">Основное направление деятельности: оптовая торговля медикаментами и сопутствующими товарами, а также дистрибуция и логистика</p>
			<p class="company-info__represent"><b>Кредо компании:</b> «Относись к человеку так, как ты хочешь, чтобы он относился к тебе». Именно поэтому компания&nbsp;Сириус Фарм известна своим исключительным вниманием к партнерам и заботой о конечном потребителе.</p>
			<p class="company-info__text">Коллектив компании Сириус Фарм верит, что искренне помогать людям можно не только адресно, но и в целом положительно влияя на индустрию, в которой организация является лидером – на рынок лекарственных препаратов и сопутствующих товаров на территории России. Другими словами, миссию компании можно обозначить как «Совершенствование и развитие отечественного рынка дистрибуции лекарств».</p>
			<b class="company-info__highlight">Особо хочется отметить обширный опыт сотрудников компании.</b>
			<p class="company-info__text">Каждый член этой слаженной команды успел поработать в разных сегментах отрасли, приобрести четкое понимание, что значит работать с хорошими или плохими поставщиками. Чтобы затем претворить в жизнь свое представление о поставщике, с которым действительно комфортно работать. Весь коллектив компании придерживается идеи построения корпоративной культуры и всех бизнес-процессов, основываясь на своем представлении о том, с каким поставщиком мы сами хотели бы вести дела.</p>
		</div>
	</section>

	<section class="features features--holding-sirius">
		<div class="features__container">
			<h3 class="features__title">Преимущества работы с компанией Сириус Фарм</h3>
			<ul class="features__list features__list--holding-sirius">
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Доступные цены.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Приоритеты компании – открытость и честность.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Стиль компании – простота в общении, отсутствие излишней бюрократии и готовность помочь в любом вопросе.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Каждому клиенту – одинаково исключительное внимание, даже самым маленьким партнерам и разовым клиентам.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Отгрузка товара осуществляется в самые короткие сроки.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Специалисты Сириус Фарм помогут разобраться во всех вопросах, касающихся реализации и дистрибуции медицинских товаров, дадут советы новичкам и смогут подарить свежие идеи даже самым опытным менеджерам.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Коллектив компании ежедневно работает над оптимизацией процессов и операций, сокращением сроков поставок, новыми решениями для типовых или нестандартных задач.</p>
				</li>
				<li class="features__item features__item--holding-sirius">
					<p class="features__text">Менеджеры по работе с клиентами идут навстречу пожеланиям по поставкам, срокам и особенностям транспортировки.</p>
				</li>
			</ul>
		</div>
	</section>

	<section class="company-info company-info--pharm">
		<div class="company-info__container">
			<picture class="company-info__logo">
				<source type="image/webp" srcset="/img/holding-logo-pharm.webp 1x, /img/holding-logo-pharm@2x.webp 2x, /img/holding-logo-pharm@3x.webp 3x">
				<img src="/img/holding-logo-pharm.png" srcset="/img/holding-logo-pharm@2x.png 2x, /img/holding-logo-pharm@3x.png 3x" width="193" height="50" alt="Логотип компании «A-Pharm»">
			</picture>
			<h2 class="company-info__title">А-Фарм</h2>
			<p class="company-info__direction">Основное направление деятельности компании – ответственное хранение&nbsp;медикаментов</p>
			<p class="company-info__represent company-info__represent--pharm">В этой сфере коллектив А-Фарм имеет более 20 лет богатейшего опыта.<br> За это время организация зарекомендовала себя как надежный партнер в фармацевтической индустрии.</p>
			<p class="company-info__text company-info__text--pharm">Компания предоставляет полный спектр услуг по складской логистике, а также по продаже лекпрепаратов оптом и в розницу. Клиенты и партнеры организации – крупнейшие компании отечественно и зарубежного рынка товаров армацевтики и медицинского назначение.</p>
			<p class="company-info__text company-info__text--pharm">Коллектив компании считает главной задачей создавать все условия для комфортного сотрудничества с А-Фарм, приоритетами в работе являются качество и надежность. Обширный опыт позволяет сотрудникам организации понимать и предвосхищать потребности клиентов, они готовы делиться своим опытом с новичками на рынке лексредств, помогая выстраивать успешную стратегию реализации лекарственных препаратов и находить эффективные решения для успешного развития.</p>
			<b class="company-info__highlight">За каждым клиентом компании А-Фарм закрепляется персональный менеджер.</b>
		</div>
	</section>

	<section class="features features--holding-pharm">
		<div class="features__container">
			<h3 class="features__title">Преимущества работы с компанией А-Фарм</h3>
			<ul class="features__list features__list--holding-pharm">
				<li class="features__item features__item--holding-pharm">
					<p class="features__text">Полный спектр логистических услуг. Прием товара, комплектование грузов, доставка клиенту и многое другое.</p>
				</li>
				<li class="features__item features__item--holding-pharm">
					<p class="features__text">Хранение грузов осуществляется на собственном лицензированном складе, с строгим соблюдением температурного режима и иных условий складирования.</p>
				</li>
				<li class="features__item features__item--holding-pharm">
					<p class="features__text">Компания несет полную материальную ответственность за сохранность доверенных ей грузов, оказывает услуги страхования.</p>
				</li>
				<li class="features__item features__item--holding-pharm">
					<p class="features__text">Широкий ассортимент компании и отточенные бизнес-процессы позволяют осуществлять отгрузку и отправку заказанного товара не более чем через 1 рабочий день (как правило, в день заказа).</p>
				</li>
				<li class="features__item features__item--holding-pharm">
					<p class="features__text">Для всех партнеров – привлекательные цены, для постоянных клиентов – гибкая система скидок.</p>
				</li>
				<li class="features__item features__item--holding-pharm">
					<p class="features__text">Наши склады имеют удобное расположение и собственную охраняемую стоянку, рассчитанную в том числе и на большегрузный транспорт. На территории ведется видеонаблюдение, охрана – круглосуточно. Все для гарантии полной безопасности хранения Вашего товара.</p>
				</li>
			</ul>
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>