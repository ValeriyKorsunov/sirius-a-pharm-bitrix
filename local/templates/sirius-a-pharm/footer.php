<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Page\Asset;

?>

</main>
<footer class="page-footer">
	<div class="page-footer__container">
		<div class="page-footer__wrapper">
			<div class="page-footer__logo">
				<a class="page-footer__logo-link">
					<picture class="page-footer__img">
						<source type="image/webp" srcset="<?=SITE_TEMPLATE_PATH?>/img/footer-logo-sirius.webp 1x, <?=SITE_TEMPLATE_PATH?>/img/footer-logo-sirius@2x.webp 2x, img/footer-logo-sirius@3x.webp 3x">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/footer-logo-sirius.png"
							 srcset="<?=SITE_TEMPLATE_PATH?>/img/footer-logo-sirius@2x.png 2x, <?=SITE_TEMPLATE_PATH?>/img/footer-logo-sirius@3x.png 3x"
							 width="83" height="28" alt="Логотип компании «Sirius»">
					</picture>
					<picture class="page-footer__img">
						<source type="image/webp"
								srcset="<?=SITE_TEMPLATE_PATH?>/img/footer-logo-pharm.webp 1x, <?=SITE_TEMPLATE_PATH?>/img/footer-logo-pharm@2x.webp 2x, img/footer-logo-pharm@3x.webp 3x">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/footer-logo-pharm.png"
							 srcset="<?=SITE_TEMPLATE_PATH?>/img/footer-logo-pharm@2x.png 2x, <?=SITE_TEMPLATE_PATH?>/img/footer-logo-pharm@3x.png 3x"
							 width="45" height="33" alt="Логотип компании «A-Pharm»">
					</picture>
				</a>
			</div>

			<div class="page-footer__copyright">
				<?if(LANGUAGE_ID=='ru'):?>
					<p class="page-footer__copyright-text">2010-2018 © ООО «Сириус-Фарм» ООО «А-ФАРМ» Продажа, дистрибуция, логистика медикаментов</p>
				<?endif;?>
				<?if(LANGUAGE_ID=='en'):?>
					<p class="page-footer__copyright-text">2010-2018 © ООО «Сириус-Фарм» ООО «А-ФАРМ» Продажа, дистрибуция, логистика медикаментов</p>
				<?endif;?>
			</div>
		</div>

		<div class="page-footer__development">
			<a class="page-footer__development-link" href="https://pixelplus.ru/">
				<?if(LANGUAGE_ID=='ru'):?>
					<img class="page-footer__development-icon" src="<?=SITE_TEMPLATE_PATH?>/img/icon-pixelplus.svg" width="34" height="36" alt="Иконка «Пиксель Плюс»">
					<span class="page-footer__development-text">Создание сайта&nbsp;– <br>компания «Пиксель Плюс»</span>
				<?endif;?>
				<?if(LANGUAGE_ID=='en'):?>
					<img class="page-footer__development-icon" src="<?=SITE_TEMPLATE_PATH?>/img/icon-pixelplus.svg" width="34" height="36" alt="Иконка «Пиксель Плюс»">
					<span class="page-footer__development-text">Создание сайта&nbsp;– <br>компания «Пиксель Плюс»</span>
				<?endif;?>
			</a>
		</div>
	</div>
</footer>

<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/picturefill.js"); ?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/all.js"></script>

</body>
</html>