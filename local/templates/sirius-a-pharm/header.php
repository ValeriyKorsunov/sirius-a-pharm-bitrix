<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();


use Bitrix\Main\Page\Asset;

?>
<!DOCTYPE html>
<html>
<head>
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle();?></title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/img/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/img/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/img/favicon-16x16.png" sizes="16x16">
	<link rel="preload" href="<?=SITE_TEMPLATE_PATH?>/fonts/opensans.woff2" as="font" crossorigin>
	<link rel="preload" href="<?=SITE_TEMPLATE_PATH?>/fonts/opensansbold.woff2" as="font" crossorigin>
	<link href="<?=SITE_TEMPLATE_PATH?>/js/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css"); ?>


</head>
<body class="no-js">
		<div id="panel" >
			<?$APPLICATION->ShowPanel();?>
		</div>

		<header class="page-header page-header--closed <?if ($_SERVER['SCRIPT_NAME'] != '/index.php'): ?> page-header--inner<? endif ?>">
			<div class="page-header__container">
				<div class="page-header__logo">
					<a href="<?=SITE_DIR?>" class="page-header__logo-link">
						<picture class="page-header__logo-img">
							<source type="image/webp" srcset="<?=SITE_TEMPLATE_PATH?>/img/logo-sirius.webp 1x, <?=SITE_TEMPLATE_PATH?>/img/logo-sirius@2x.webp 2x, <?=SITE_TEMPLATE_PATH?>/img/logo-sirius@3x.webp 3x">
							<img src="<?=SITE_TEMPLATE_PATH?>/img/logo-sirius.png" srcset="<?=SITE_TEMPLATE_PATH?>/img/logo-sirius@2x.png 2x, <?=SITE_TEMPLATE_PATH?>/img/logo-sirius@3x.png 3x"
								 width="120" height="40" alt="Логотип компании «Sirius»">
						</picture>
						<picture class="page-header__logo-img">
							<source type="image/webp" srcset="<?=SITE_TEMPLATE_PATH?>/img/logo-pharm.webp 1x, <?=SITE_TEMPLATE_PATH?>/img/logo-pharm@2x.webp 2x, <?=SITE_TEMPLATE_PATH?>/img/logo-pharm@3x.webp 3x">
							<img src="<?=SITE_TEMPLATE_PATH?>/img/logo-pharm.png" srcset="<?=SITE_TEMPLATE_PATH?>/img/logo-pharm@2x.png 2x, <?=SITE_TEMPLATE_PATH?>/img/logo-pharm@3x.png 3x"
								 width="64" height="47" alt="Логотип компании «A-Pharm»">
						</picture>
					</a>
				</div>

				<button class="page-header__toggle" type="button">
					<span class="page-header__toggle-line"></span>
					<span class="visually-hidden">Открыть меню</span>
				</button>

				<ul class="language-list">
					<li class="language-list__item">
						<a class="language-list__link <?if(LANGUAGE_ID=='ru'):?>language-list__link--active<?endif;?>" href="/">
							Ru</a>
					</li>
					<li class="language-list__item">
						<a class="language-list__link <?if(LANGUAGE_ID=='en'):?>language-list__link--active<?endif;?>" href="/en/">En</a>
					</li>
				</ul>

				<?$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multilevel", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "2",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"COMPONENT_TEMPLATE" => "horizontal_multilevel",
		"MENU_THEME" => "site"
	),
	false
);?>

				<div class="header-contacts">
					<a class="header-contacts__phone" href="tel:<?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", array(), array(SHOW_BORDER  => false)); ?>">
						<?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", array(), array(MODE => "text")); ?>
					</a>
					<a class="header-contacts__email" href="mailto:<?$APPLICATION->IncludeFile(SITE_DIR."include/email-1.php", array(), array(SHOW_BORDER  => false)); ?>">
						<?$APPLICATION->IncludeFile(SITE_DIR."include/email-1.php", array(), array(MODE => "text")); ?>
					</a>
					<a class="header-contacts__email" href="mailto:<?$APPLICATION->IncludeFile(SITE_DIR."include/email-2.php", array(), array(SHOW_BORDER  => false)); ?>">
						<?$APPLICATION->IncludeFile(SITE_DIR."include/email-2.php", array(), array(MODE => "text")); ?>
					</a>
				</div>
			</div>
			<?if ($_SERVER['SCRIPT_NAME'] == '/index.php'): ?>
				<section class="promo">
					<div class="promo__container">
						<div class="promo__wrapper">
							<p class="promo__text"><?$APPLICATION->IncludeFile(SITE_DIR."include/promo.php", array(), array(MODE => "text")); ?></p>
						</div>
					</div>
				</section>
			<? endif ?>
		</header>
		<main class="page-main">
