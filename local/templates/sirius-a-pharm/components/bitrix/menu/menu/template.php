<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<ul class="site-list">
		<li class="site-list__item">
			<a class="site-list__link" href="<?=SITE_DIR?>">
				<img class="site-list__icon" src="<?=SITE_TEMPLATE_PATH?>/img/icon-home.svg" width="15" height="14" alt="На главную">
			</a>
		</li>
		<?foreach($arResult as $arItem):
			if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
			<li class="site-list__item">
				<a class="site-list__link <?if($arItem["SELECTED"]) echo "active";?>" href="<?=$arItem["LINK"]?>">
					<?=$arItem["TEXT"]?>
				</a>
			</li>
		<?endforeach?>

	</ul>
<?endif?>