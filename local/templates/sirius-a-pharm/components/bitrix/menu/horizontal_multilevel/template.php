<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<ul class="site-list">
	<li class="site-list__item">
		<a class="site-list__link" href="<?=SITE_DIR?>">
			<img class="site-list__icon" src="<?=SITE_TEMPLATE_PATH?>/img/icon-home.svg" width="15" height="14" alt="На главную">
		</a>
	</li>
<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="site-list__item site-list__item--dropdown" tabindex="0">
				<button class="site-list__toggle" type="button">
			    	<span class="visually-hidden">Раскрыть раздел</span>
			    </button>
				<a <?if($arItem["PARAMS"]["NO_PAGE"]!='Y'):?> href="<?=$arItem["LINK"]?>" <?endif;?> class="site-list__link <?if ($arItem["SELECTED"]):?> active<?endif?>" >	<?=$arItem["TEXT"]?>
				</a>
				<ul class="site-list__item-list sub-nav">
		<?else:?>
			<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
				<ul>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="site-list__item">
					<a href="<?=$arItem["LINK"]?>" class="site-list__link <?if ($arItem["SELECTED"]):?>active<?endif?>">
						<?=$arItem["TEXT"]?>
					</a>
				</li>
			<?else:?>
				<li class="sub-nav__item"><a class="sub-nav__link <?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="site-list__item">
					<a href="<?=$arItem["LINK"]?>" class="site-list__link <?if ($arItem["SELECTED"]):?>active<?endif?>">
						<?=$arItem["TEXT"]?>
					</a>
				</li>
			<?else:?>
				<li class="sub-nav__item"><a class="sub-nav__link <?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<?endif?>