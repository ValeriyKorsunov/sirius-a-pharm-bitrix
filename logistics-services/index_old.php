<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Схема проезда на склад ООО «ЖиванаХол»");
?><h1>Поиск сертификатов</h1>

<?
$options = array(
    'SPB' => 'Санкт-Петербург',
    'MSK' => 'Москва',
    'STV' => 'Ставрополь',
);

?>
<form class="form_sert" method="post" action="">
    <ul>
        <li>
            <label for="">
                <b>Документ</b>
            </label>
            <select type="text" name="doc" id="docs_select">
                <option value="s4et-factura">Счет-фактура</option>
                <option value="nakladnaya">Накладная</option>
            </select>
        </li>
        <li id="s4et-factura">
            <label for="">
                <b>Номер счета-фактуры</b>
            </label>
            <input type="text" name="number" value="<?=isset($_POST['number']) ? $_POST['number'] : '' ?>">
			<br><br>
			<p><small>
				Пожалуйста, вводите только последние цифры из номер документа. Нули и буквы вводить не нужно.
				<br>
				Пример: накладная тжс00000011, необходимо  вводить: 11
				</small></p>
        </li>
        <li id="nakladnaya" style="display: none;">
            <label for="">
                <b>Номер накладной</b>
            </label>
            <input type="text" name="code" value="<?=isset($_POST['code']) ? $_POST['code'] : '' ?>">
			<br><br>
			<p><small>
				Пожалуйста, вводите только последние цифры из номер документа. Нули и буквы вводить не нужно.
				<br>
				Пример: накладная тжс00000011, необходимо  вводить: 11
				</small></p>
        </li>
        <input type="hidden" name="city" id="city_select" value="SPB">
        <?/*li>
            <label for="">
            	<b>Филиал</b>
			</label>
			<select type="text" name="city" id="city_select"
                                                              value="<?=isset($_POST['city']) ? $_POST['city'] : '' ?>">

                <? foreach ($options as $key => $option) : ?>
                    <option
                        value="<?= $key ?>" <? if (isset($_POST['city']) && $key == $_POST['city']) echo 'selected' ?>><?= $option ?>
                    </option>
                <? endforeach ?>
            </select>
        </li*/?>
        <li>
            <label for=""><b>Дата</b></label>
            <input type="text" id="datepicker" name="date" value="<?=isset($_POST['date']) ? $_POST['date'] : '' ?>">
        </li>
        <li>
        	<label for=""></label>
            <input type="submit" name="submit" class="submit" value="Найти">
        </li>
</form>

<? include_once('result.php') ?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>