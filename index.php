<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Главная");
?>

	<section class="features features--index">
		<div class="features__container">
			<h2 class="visually-hidden">Наши преимущества</h2>
			<a class="btn features__btn" href="https://www.youtube.com/watch?v=BXlMfS4waEA" data-video>
				<img class="features__btn-icon" src="img/icon-video.svg" width="30" height="30" alt="Иконка воспроизведения видео">
				Видео-презентация
			</a>
			<p class="features__info">В 2018 году А-Фарм и Сириус Фарм объединились в единую структуру, что позволило группе компаний предложить своим клиентам не только привычно безупречное качество медикаментов, но и широчайшие возможности по реализации, доставке и дистрибуции товаров медицинского назначения.</p>
			<p class="features__info">Данная консолидация означает как юридическое объединение, так и объединение ресурсов, возможностей и, самое главное, опыта двух ведущих компаний, осуществляющих деятельность на отечественном фармацевтическом рынке уже более 20 лет.</p>
			<ul class="features__list">
				<li class="features__item features__item--index">
					<p class="features__text">В ассортименте – продукция крупнейших фармацевтических компаний России, Европы и всего мира</p>
				</li>
				<li class="features__item features__item--index">
					<p class="features__text">Автоматизация бизнес-процессов позволяет минимизировать наценку и сделать продукцию доступнее</p>
				</li>
				<li class="features__item features__item--index">
					<p class="features__text">Собственные склады и автопарки по всей России, хранение и доставка грузов любой сложности</p>
				</li>
				<li class="features__item features__item--index">
					<p class="features__text">Квалифицированный персонал с многолетним опытом работы в коммерческом и бюджетном сегментах</p>
				</li>
				<li class="features__item features__item--index">
					<p class="features__text">Участие в федеральной социальной программе льготного обеспечения лекарственными препаратами</p>
				</li>
			</ul>
		</div>
	</section>

	<section class="clients">
		<div class="clients__container">
			<h2 class="clients__title">Наши клиенты</h2>
			<ul class="clients__list">
				<li class="clients__item clients__item--first-group">
					<a class="clients__link" href="#">
						<picture>
							<source type="image/webp" srcset="img/client-36.webp 1x, img/client-36@2x.webp 2x, img/client-36@3x.webp 3x">
							<img src="img/client-36.png" srcset="img/client-36@2x.png 2x, img/client-36@3x.png 3x" width="89" height="89" alt="Логотип компании «36,6»">
						</picture>
					</a>
				</li>
				<li class="clients__item clients__item--first-group">
					<a class="clients__link" href="#">
						<picture>
							<source type="image/webp" srcset="img/client-grand.webp 1x, img/client-grand@2x.webp 2x, img/client-grand@3x.webp 3x">
							<img src="img/client-grand.png" srcset="img/client-grand@2x.png 2x, img/client-grand@3x.png 3x" width="147" height="64" alt="Логотип компании «Гранд Аптека»">
						</picture>
					</a>
				</li>
				<li class="clients__item clients__item--first-group">
					<a class="clients__link" href="#">
						<picture>
							<source type="image/webp" srcset="img/client-gorzdrav.webp 1x, img/client-gorzdrav@2x.webp 2x, img/client-gorzdrav@3x.webp 3x">
							<img src="img/client-gorzdrav.png" srcset="img/client-gorzdrav@2x.png 2x, img/client-gorzdrav@3x.png 3x" width="173" height="49" alt="Логотип компании «Горздрав»">
						</picture>
					</a>
				</li>
				<li class="clients__item clients__item--second-group">
					<a class="clients__link" href="#">
						<picture>
							<source type="image/webp" srcset="img/client-apteka.ru.webp 1x, img/client-apteka.ru@2x.webp 2x, img/client-apteka.ru@3x.webp 3x">
							<img src="img/client-apteka.ru.png" srcset="img/client-apteka.ru@2x.png 2x, img/client-apteka.ru@3x.png 3x" width="90" height="91" alt="Логотип компании «Apteka.ru»">
						</picture>
					</a>
				</li>
				<li class="clients__item clients__item--second-group">
					<a class="clients__link" href="#">
						<picture>
							<source type="image/webp" srcset="img/client-dialog.webp 1x, img/client-dialog@2x.webp 2x, img/client-dialog@3x.webp 3x">
							<img src="img/client-dialog.png" srcset="img/client-dialog@2x.png 2x, img/client-dialog@3x.png 3x" width="167" height="44" alt="Логотип компании «Диалог»">
						</picture>
					</a>
				</li>
				<li class="clients__item clients__item--second-group">
					<a class="clients__link" href="#">
						<picture>
							<source type="image/webp" srcset="img/client-a5.webp 1x, img/client-a5@2x.webp 2x, img/client-a5@3x.webp 3x">
							<img src="img/client-a5.png" srcset="img/client-a5@2x.png 2x, img/client-a5@3x.png 3x" width="165" height="34" alt="Логотип компании «А5 Аптека»">
						</picture>
					</a>
				</li>
			</ul>
		</div>
	</section>

	<section class="services">
		<div class="services__container">
			<h2 class="services__title">Услуги</h2>
			<ul class="services__list">
				<li class="services__item">
					<h3 class="services__item-title">Оптовые продажи медикаментов</h3>
					<p class="services__text">Оптовые поставки медикаментов - основное направление деятельности организации. Имея обширный опыт работы на рынке фармацевтических препаратов, группа компаний А-Фарм и Сириус Фарм готова предложить своим партнерам превосходные условия сотрудничества, гибкую ценовую политику, индивидуальные скидки.</p>
				</li>
				<li class="services__item">
					<h3 class="services__item-title">Складская <br>логистика</h3>
					<p class="services__text">Лицензированные фармацевтические склады класса А и В+ по всей территории РФ позволяют подобрать точки хранения товаров под нужды клиента - с оптимальным расположением, позволяющие максимально эффективно решать задачи логистики и сокращать время доставки.</p>
				</li>
				<li class="services__item">
					<h3 class="services__item-title">Логистическая <br>служба</h3>
					<p class="services__text">Собственная логистическая служба – быстрая и качественная доставка грузов любой сложности. Каждый этап сопровождения грузов тщательно контролируется. Даже самый сложный груз будет доставлен точно в срок, с соблюдением всех условий перевозки, в том числе температурного режима. Для этих целей используется собственный автопарк грузовиков с самой современной климатической установкой.</p>
				</li>
				<li class="services__item">
					<h3 class="services__item-title">Дистрибуция медицинских препаратов</h3>
					<p class="services__text">Услуги по регистрации и продвижению препаратов: во всех регионах РФ, на рынках дальнего зарубежья – всего более 80 стран мира. Полный спектр услуг – от растаможивания, получения всех необходимых лицензий и сертификатов до создания локального бренда и выведения на местный рынок через аптечные сети и ретейлеров.</p>
				</li>
				<li class="services__item">
					<h3 class="services__item-title">Участие в бюджетных программах</h3>
					<p class="services__text">Участие в бюджетных программах по льготному обеспечению лекарственными препаратами. Поддержка социально слабых групп населения, а также обеспечение доступными и качественными медикаментами всего населения РФ.</p>
				</li>
				<li class="services__item">
					<h3 class="services__item-title">Таможенное <br>оформление</h3>
					<p class="services__text">Компания оказывает любые виды услуг таможенного оформления, помогает полностью оформить сделку от начала и ее завершения, а также предлагает комплексное сопровождение внешнеторговых контрактов любой сложности.</p>
				</li>
			</ul>
		</div>
	</section>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>