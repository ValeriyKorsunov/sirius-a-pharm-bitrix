<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О холдинге");
?>

<section class="inner-heading">
	<div class="inner-heading__container">
		<h1 class="inner-heading__title">COMPANY</h1>
		<p class="type-page__text">
			We believe in the golden rule «Treat others as you want to be treated» and we sincerely want to help others. That is why we are very attentive towards our partners and end consumers. Each member of our team had an opportunity to work in different sections of this branch and to get the idea of what it is like to work with good and bad suppliers. We create corporate culture and all business processes basing on our own experience and idea of the supplier we would prefer to work with.
		</p>
		<p class="type-page__text">
			<b>Advantages of working with us:</b>
		</p>
		<ul class="type-page__list">
    		<li class="type-page__item">
    			<p class="type-page__item-text">affordable prices</p>
    		</li>
    		<li class="type-page__item">
    			<p class="type-page__item-text">short term dispatching of goods</p>
    		</li>
    		<li class="type-page__item">
    			<p class="type-page__item-text">our priorities – openness and honesty</p>
    		</li>
    		<li class="type-page__item">
    			<p class="type-page__item-text">Our customers appreciate us for communication simplicity and willingness to help with any issue.</p>
    		</li>
    		<li class="type-page__item">
    			<p class="type-page__item-text">We are attentive to everybody, even to small and one-time customers</p>
    		</li>
    		<li class="type-page__item">
    			<p class="type-page__item-text">We help our partners to deal with all the issues, give advice to beginners and give new ideas to your skilled managers</p>
    		</li>
    		<li class="type-page__item">
    			<p class="type-page__item-text">We meet your needs concerning deliveries, terms, and shipping peculiarities.</p>
    		</li>
    		<li class="type-page__item">
    			<p class="type-page__item-text">We appreciate our and our customers' time. We are constantly look for new solutions to standard problems, try to optimize all the processes and procedures, to reduce delivery terms.</p>
    		</li>
		</ul>
		<h2 class="type-page__title">HISTORY OF THE COMPANY</h2>
		<p class="type-page__text">
			Sirius Company is the union of professionals knowing medical product industry from the inside. They had an opportunity to work in different sections of this branch. Acquired experience and detailed knowledge of issue allows us to understand our partners' needs and do our best for the cooperation to be nice. We've become the big regional distributor owing to professionalism and diligence of our team. There are more than 500 buyers among our regular customers – they are the biggest regional companies and national distributors.
		</p>
	</div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>