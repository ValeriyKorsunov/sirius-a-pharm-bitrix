<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?>

	<section class="job">
		<div class="job__container">
			<h1 class="job__title">Вакансии</h1>
			<p class="job__text">Так как наша компания постоянно развивается и расширяется, нам требуются новые, сильные сотрудники.</p>
			<p class="job__text">Политика компании в том, что мы «выращиваем» руководителей из своих сотрудников, а не нанимаем их со стороны, поэтому в нашей компании всегда есть все возможности для вашего карьерного роста.</p>
			<a class="job__link" href="https://hh.ru">
				<picture>
					<source type="image/webp" srcset="/img/logo-hh.webp 1x, img/logo-hh@2x.webp 2x, img/logo-hh@3x.webp 3x">
					<img src="/img/logo-hh.png" srcset="/img/logo-hh@2x.png 2x, img/logo-hh@3x.png 3x" width="158" height="87" alt="Логотип компании «Head Hunter»">
				</picture>
				<span class="job__link-text">Вакансии</span>
			</a>
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>