<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сертификаты ");
?>

	<section class="certificate">
		<div class="certificate__container">
			<h1 class="certificate__title">Поиск сертификатов</h1>
			<form class="certificate__form" method="post" action="/wholesale-supply/certificates/">
				<span class="certificate__label">Тип документа:</span>
				<div class="certificate__select">
					<span class="certificate__select-value certificate__select-value--active">Счет-фактура</span>
					<ul class="certificate__select-dropdown">
						<li class="certificate__select-option" data-value="invoice">Счет-фактура</li>
						<li class="certificate__select-option" data-value="waybill">Накладная</li>
					</ul>
					<input type="hidden" name="doc" id="type">
				</div>
				<div class="certificate__field-number certificate__field-number--invoice certificate__field-number--active">
					<label class="certificate__label" for="invoice-number">Номер счета-фактуры</label>
					<input class="certificate__field" id="invoice-number" type="number" name="number" placeholder=""
						   value="<?=isset($_POST['number']) ? $_POST['number'] : '' ?>">
				</div>
				<div class="certificate__field-number certificate__field-number--waybill">
					<label class="certificate__label" for="waybill-number">Номер накладной</label>
					<input class="certificate__field" id="waybill-number" type="text" name="code" placeholder=""
						   value="<?=isset($_POST['code']) ? $_POST['code'] : '' ?>">
				</div>
				<p class="certificate__sample">Пожалуйста, вводите только последние цифры из номера документа. Нули и буквы вводить не нужно. Пример: накладная тжс00000011, необходимо вводить: 11</p>
				<input type="hidden" name="city" id="city_select" value="SPB">
				<label class="certificate__label" for="date">Дата</label>
				<input name="date" class="certificate__field certificate__field--calendar" type="text"
					   value="<?=isset($_POST['date']) ? $_POST['date'] : '' ?>" size="10" onClick="xCal(this)"
					   size="10" onmouseenter="xCal(this)" onKeyUp="xCal()" placeholder="24.09.2012" required="">
				<button class="btn cartificate__btn" type="submit" name="submit" class="btn">Найти</button>
			</form>
			<br>
			<? include_once('result.php') ?>
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>