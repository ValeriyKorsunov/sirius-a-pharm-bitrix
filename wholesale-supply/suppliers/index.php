<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поставщикам");
?>

	<section class="inner-heading">
		<div class="inner-heading__container">
			<h1 class="inner-heading__title">Поставщикам</h1>
			<p class="inner-heading__text">Группа компаний А-Фарм и Сириус Фарм приглашает к сотрудничеству поставщиков лекарственных препаратов и медицинских товаров из всех регионов РФ. Холдинг предлагает своим партнерам широкий спектр услуг, превосходные условия работы и оказание поддержки по любым вопросам. Наша миссия - совершенствование и развитие рынка дистрибуции лекарств в России. Все наши силы и ресурсы мы направляем на достижение этой цели.</p>
		</div>
	</section>

	<section class="features features--suppliers">
		<div class="features__container">
			<h2 class="features__title">Преимущества работы с группой компаний А-Фарм и Сириус Фарм:</h2>
			<ul class="features__list features__list--suppliers">
				<li class="features__item features__item--suppliers">
					<p class="features__text">Группа компаний обеспечивает эффективную дистрибуцию товаров своих клиентов – в любом объеме, в любом&nbsp;регионе РФ и СНГ</p>
				</li>
				<li class="features__item features__item--suppliers">
					<p class="features__text">Сеть собственных складов по всей стране – ответственное хранение с соблюдением всех условий, поддержание стабильного запаса широкого ассортимента наименований товаров, удобная оперативная логистика</p>
				</li>
				<li class="features__item features__item--suppliers">
					<p class="features__text">Собственная логистическая служба - высокое качество услуг, надежность логистических операций, контроль каждого этапа обработки доверенных компании грузов</p>
				</li>
				<li class="features__item features__item--suppliers">
					<p class="features__text">Тщательность во взаиморасчетах и документообороте, консалтинг по всем смежным вопросам</p>
				</li>
				<li class="features__item features__item--suppliers">
					<p class="features__text">Партнеры холдинга – крупнейшие аптечные сети и оптовики РФ, что означает широчайшие возможности для динамичного развития бизнеса</p>
				</li>
			</ul>
		</div>
	</section>

	<section class="trade-sectors">
		<div class="trade-sectors__container">
			<h2 class="trade-sectors__title">Ресурсы, которыми располагает группа компаний А-Фарм и Сириус Фарм, позволяют успешно реализовывать многоканальную<br> дистрибуцию в следующих сферах:</h2>
			<ul class="trade-sectors__list">
				<li class="trade-sectors__item">
					<picture>
						<source type="image/webp" srcset="/img/sector-wholesale.webp 1x, /img/sector-wholesale@2x.webp 2x, /img/sector-wholesale@3x.webp 3x">
						<img src="/img/sector-wholesale.jpg" srcset="/img/sector-wholesale@2x.jpg 2x, /img/sector-wholesale@3x.jpg 3x" width="220" height="340" alt="Изображение сферы дистрибуции">
					</picture>
					<span class="trade-sectors__text">Фармацевтический опт</span>
				</li>
				<li class="trade-sectors__item">
					<picture>
						<source type="image/webp" srcset="/img/sector-retail.webp 1x, /img/sector-retail@2x.webp 2x, /img/sector-retail@3x.webp 3x">
						<img src="/img/sector-retail.jpg" srcset="/img/sector-retail@2x.jpg 2x, /img/sector-retail@3x.jpg 3x" width="220" height="340" alt="Изображение сферы дистрибуции">
					</picture>
					<span class="trade-sectors__text">Аптечный ритейл</span>
				</li>
				<li class="trade-sectors__item">
					<picture>
						<source type="image/webp" srcset="/img/sector-health.webp 1x, /img/sector-health@2x.webp 2x, /img/sector-health@3x.webp 3x">
						<img src="/img/sector-health.jpg" srcset="/img/sector-health@2x.jpg 2x, /img/sector-health@3x.jpg 3x" width="220" height="340" alt="Изображение сферы дистрибуции">
					</picture>
					<span class="trade-sectors__text">Департаменты здравоохранения</span>
				</li>
				<li class="trade-sectors__item">
					<picture>
						<source type="image/webp" srcset="/img/sector-fea.webp 1x, /img/sector-fea@2x.webp 2x, /img/sector-fea@3x.webp 3x">
						<img src="/img/sector-fea.jpg" srcset="/img/sector-fea@2x.jpg 2x, /img/sector-fea@3x.jpg 3x" width="220" height="340" alt="Изображение сферы дистрибуции">
					</picture>
					<span class="trade-sectors__text">ВЭД</span>
				</li>
				<li class="trade-sectors__item">
					<picture>
						<source type="image/webp" srcset="/img/sector-fmgg.webp 1x, /img/sector-fmgg@2x.webp 2x, /img/sector-fmgg@3x.webp 3x">
						<img src="/img/sector-fmgg.jpg" srcset="/img/sector-fmgg@2x.jpg 2x, /img/sector-fmgg@3x.jpg 3x" width="220" height="340" alt="Изображение сферы дистрибуции">
					</picture>
					<span class="trade-sectors__text">FMCG&#8209;реализация товаров&nbsp;массового потребления</span>
				</li>
			</ul>
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>