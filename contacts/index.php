<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

	<section class="contacts">
		<div class="contacts__container">
			<h1 class="contacts__title">Контакты</h1>
			<ul class="contacts__list">
				<li class="contacts__item">
					<h2 class="contacts__item-title">Адрес</h2>
					<p class="contacts__info">г.Москва, ул. Верейская, д. 29, стр. 136</p>
				</li>
				<li class="contacts__item">
					<h2 class="contacts__item-title">Телефон</h2>
					<a class="contacts__info contacts__info--tel" href="tel:+7(495)2155074"> +7 (495) 215-50-74</a>
				</li>
				<li class="contacts__item">
					<h2 class="contacts__item-title">Адрес</h2>
					<p class="contacts__info">г.Москва, ул. Верейская, д. 29, стр. 136</p>
				</li>
				<li class="contacts__item">
					<h2 class="contacts__item-title">Реквезиты</h2>
					<a class="contacts__info contacts__info--download" href="#" download=""> Sirius-pharm</a>
					<a class="contacts__info contacts__info--download" href="#" download="">A-pharm</a>
				</li>
			</ul>
		</div>
		<div class="contacts__map"></div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>